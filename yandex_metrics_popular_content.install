<?php
/**
 * @file
 * Install, uninstall and update the module.
 */

/**
 * Implements hook_uninstall().
 */
function yandex_metrics_popular_content_uninstall() {
  // Delete module variables and clear variables cache.
  db_delete('variable')
    ->condition('name', 'yandex_metrics_popular_content_%', 'LIKE')
    ->execute();
  cache_clear_all('variables', 'cache_bootstrap');
}

/**
 * Implements hook_schema().
 */
function yandex_metrics_popular_content_schema() {
  $schema['yandex_metrics_popular_content_data'] = array(
    'description' => 'Stores the popular content.',
    'fields' => array(
      'yid' => array(
        'description' => 'The id for url.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'url' => array(
        'description' => 'The url obtained from Yandex.metrika.',
        'type' => 'varchar',
        'length' => 2048,
        'not null' => TRUE,
        'default' => ''
      ),
      'language' => array(
        'description' => 'Language of the page.',
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
        'default' => ''
      ),
      'page_title' => array(
        'description' => 'The page title of the url.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'page_views' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Page views of this url.',
      )
    ),
    'primary key' => array('yid'),
    'indexes' => array(
      'language' => array('language'),
      'url' => array(array('url', 255)),
    ),
  );

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function yandex_metrics_popular_content_requirements($phase) {
  $t = get_t();
  $requirements = array();

  if ($phase == 'runtime') {
    if (function_exists('yandex_services_auth_info') && !yandex_services_auth_info('token')) {
      $message = $t('Please, <a href="!module_config_url">configure @module</a> module.', array('!module_config_url' => url('admin/config/system/yandex_services_auth'), '@module' => 'Yandex Services Authorization API'));

      $requirements['yandex_metrics_popular_content'] = array(
        'title' => t('Yandex.Metrics Popular Content'),
        'value' => $message,
        'severity' => REQUIREMENT_WARNING,
      );
    }
  }

  return $requirements;
}
