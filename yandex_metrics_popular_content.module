<?php
/**
 * @file
 * The main code of Yandex.Metrics Popular Content module.
 */

/**
 * Quantity of popular content lines.
 */
define('YANDEX_METRICS_POPULAR_CONTENT_COUNT', 30);

/**
 * Max count of links in Popular Content block.
 */
define('YANDEX_METRICS_POPULAR_CONTENT_BLOCK_LIMIT', 50);

/**
 * Implements hook_menu().
 */
function yandex_metrics_popular_content_menu() {

  $items['admin/config/system/yandex_metrics/cron'] = array(
    'access arguments' => array('administer Yandex.Metrics settings'),
    'page callback'   => 'drupal_get_form',
    'page arguments'  => array('yandex_metrics_popular_content_cron_settings'),
    'title'           => 'Cron',
    'type' => MENU_LOCAL_TASK,
    'weight' => 4
  );

  return $items;
}

/**
 * Implements hook_views_api().
 */
function yandex_metrics_popular_content_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'yandex_metrics_popular_content') . '/views',
  );
}

/**
 * Fetch Popuplar content from Yandex.metrika and save it to the database.
 * @param bool $cron - Indicates that we run function in cron or another place.
 */
function yandex_metrics_popular_content_save_popular_content($cron = FALSE) {
  $counter_code = variable_get('yandex_metrics_counter_code', '');
  if (empty($counter_code) && !$cron) {
    drupal_set_message(
      t('Perhaps you have not yet placed Yandex.Metrica counter code on the site. You can do this !link.', array('!link' => l(t('here'), 'admin/settings/yandex_metrics'))),
      'notice'
    );
    return;
  }

  $counter_id = yandex_metrics_reports_get_counter_for_current_site();
  if (empty($counter_id)) {
    if ($cron) {
      watchdog('yandex_metrics_popular_content', 'Cron: counter ID is not set.', array(), WATCHDOG_WARNING);
    }
    else {
      drupal_set_message(
        t('Please create Yandex.Metrica counter for the site first. See more details !link.', array('!link' => l(t('here'), 'admin/config/system/yandex_metrics'))),
        'error'
      );
    }
    return;
  }

  $authorisation_token = yandex_services_auth_info('token');
  if (empty($authorisation_token)) {
    if ($cron) {
      watchdog('yandex_metrics_popular_content', 'Cron: application is not authorised.', array(), WATCHDOG_WARNING);
    }
    else {
      drupal_set_message(
        t('Please make sure that your application is authorized !link.', array('!link' => l(t('here'), 'admin/settings/yandex_metrics/authorization'))),
        'error'
      );
    }

    return;
  }

  $filter = variable_get('yandex_metrics_popular_content_date_period', 'week');
  $date_range = yandex_metrics_reports_filter_to_date_range($filter);

  $parameters = array(
    'id' => $counter_id,
    'date1' => $date_range['start_date'],
    'date2' => $date_range['end_date']
  );

  // Fetch popular content urls.
  $report_content = yandex_metrics_reports_retreive_data('/stat/content/popular', $parameters);
  $content = json_decode($report_content->data);

  // Check for errors.
  if (isset($content->errors) && count($content->errors) > 0 && $content->errors[0]->code != 'ERR_NO_DATA') {
    watchdog(
      'yandex_metrics_popular_content',
      'Fetching of popular content failed due to error %error.',
      array('%error' => $content->errors[0]->text),
      WATCHDOG_ERROR
    );
    return;
  }

  // Remove outdated data.
  db_truncate('yandex_metrics_popular_content_data')->execute();

  if (empty($content->data)) {
    watchdog(
      'yandex_metrics_popular_content',
      'There is no popular content for the selected date period %filter.',
      array('%filter' => $filter),
      WATCHDOG_NOTICE
    );
    return;
  }

  $counter = 1;
  foreach ($content->data as $value) {

    $parsed_url = parse_url($value->url);

    // Ignore external urls.
    // Let's consider third level domains as internal, e.g. es.domain.com.
    global $base_root;
    $base_root_parts = parse_url($base_root);
    $pattern = "/(\.|^)" . preg_quote($base_root_parts['host'], "/") . "$/i";
    if (!preg_match($pattern, $parsed_url['host'], $matches)) {
      continue;
    }

    // Obtain page language.
    $page_language = yandex_metrics_popular_content_language_from_url($value->url);

    // Obtain page title if it is possible.
    $page_title = yandex_metrics_popular_content_obtain_page_title($parsed_url['path'], $page_language);

    db_insert('yandex_metrics_popular_content_data')
      ->fields(array(
        'url' => $value->url,
        'language' => $page_language,
        'page_title' => $page_title,
        'page_views' => $value->page_views,
      ))
      ->execute();

    if ($counter++ >= YANDEX_METRICS_POPULAR_CONTENT_BLOCK_LIMIT) {
      break;
    }
  }

  watchdog('yandex_metrics_popular_content', 'Popular content for %filter has been fetched.', array('%filter' => $filter), WATCHDOG_NOTICE);
}

/**
 * Obtain page title by path and language if possible.
 *
 * @param string $path
 * @param string $language
 *
 * @return string
 */
function yandex_metrics_popular_content_obtain_page_title($path, $language = 'en') {

  $path = ltrim($path, '/');

  $site_frontpage = variable_get('site_frontpage', 'node');

  if (empty($path) || $path == $site_frontpage || $path == $language . '/' . $site_frontpage) {
    return variable_get('site_name', t('Front page', array(), array('langcode' => $language)));
  }

  // Get normal path, like node/1.
  $normal_path = drupal_get_normal_path($path, $language);

  $item = menu_get_item($normal_path);

  if (empty($item) || empty($item['title'])) {
    return $path;
  }

  return $item['title'];
}

/**
 * Implements hook_cron().
 */
function yandex_metrics_popular_content_cron() {
  // Skip cron execution if Popular Content block is not enabled.
//  if (!_yandex_metrics_popular_content_is_popular_content_enabled()) {
//    return;
//  }

  yandex_metrics_popular_content_save_popular_content(TRUE);
}

/**
 * Implements hook_help().
 */
function yandex_metrics_popular_content_help($path, $arg) {
  switch ($path) {
    case 'admin/help#yandex_metrics_popular_content':
      $output = '';
      $output .= '<h3>' . t('About the module') . '</h3>';
      $output .= '<p>' . t('The <a href="@yandex_metrika" target="_blank">Yandex.Metrica</a> service is European alternative of Google Analytics. This is a free tool that helps you to increase the conversion rate of your site.', array('@yandex_metrika' => 'http://metrika.yandex.ru/')) . '</p>';
      $output .= '<p>' . t('The Yandex.Metrics Popular Content module provides <a href="@popular-content-view-page">Popular Content view</a>.', array('@popular-content-view-page' => url('admin/structure/views/view/popular_content'))) . '</p>';
      $output .= '<h3>' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Configuring cron') . '</dt>';
      $output .= '<dd>' . t('You can configure cron settings for the module, such as date period of data for <a href="@popular-content-view-page">Popular Content view</a>. To do it go to <a href="@cron-setting-page">Cron settings page</a>.', array('@cron-setting-page' => url('admin/config/system/yandex_metrics/cron'), '@popular-content-view-page' => url('admin/structure/views/view/popular_content'))) . '</dd>';
      $output .= '</dl>';
      return $output;
    case 'admin/config/system/yandex_metrics/cron':
      $output = '<p>' . t('Here you can specify settings which are used in cron.') . '</p>';
      return $output;
   }
}

/*
 * Check whether Popular Content view and block are enabled.
 */
function _yandex_metrics_popular_content_is_popular_content_enabled() {
  $result = db_select("block")
    ->condition("module", "yandex_metrics_reports")
    ->condition("delta", "popular_content")
    ->condition("status", 1)
    ->fields('block')
    ->execute()->fetchField();

  $views = views_get_view('popular_content');
  // if views is present and it`s enabled
  if (!empty($views) && !$views->disabled) {
    $result = 1;
  }

  return !empty($result);
}

/**
 * Menu callback; cron settings form.
 */
function yandex_metrics_popular_content_cron_settings() {

  $form['popular_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Popular Content Block Data')
  );

  $options = array(
    'day' => t('Today'),
    'yesterday' => t('Yesterday'),
    'week' => t('Week'),
    'month' => t('Month')
  );
  $form['popular_content']['yandex_metrics_popular_content_date_period'] = array(
    '#type' => 'select',
    '#title' => t('Date period'),
    '#description' => t('This date period is used to fetch popular content from Yandex.Metrica by cron.'),
    '#options' => $options,
    '#default_value' => variable_get('yandex_metrics_popular_content_date_period', 'week'),
  );

  return system_settings_form($form);
}

/**
 * Identify language via URL prefix or domain.
 *
 * @see locale_language_from_url().
 *
 * @param $url
 *   A URL.
 *
 * @return
 *   A valid language code.
 */
function yandex_metrics_popular_content_language_from_url($url) {

  // Include necessary libraries.
  require_once DRUPAL_ROOT . '/includes/locale.inc';
  require_once DRUPAL_ROOT . '/includes/language.inc';

  $default_language = language_default();

  $url_language = $default_language->language;

  if (!language_negotiation_get_any(LOCALE_LANGUAGE_NEGOTIATION_URL)) {
    return $url_language;
  }

  $languages = language_list();

  $url_parts = parse_url($url);

  $host = $url_parts['host'];
  $path = ltrim($url_parts['path'], '/');

  switch (variable_get('locale_language_negotiation_url_part', LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX)) {
    case LOCALE_LANGUAGE_NEGOTIATION_URL_PREFIX:
      list($language, $path) = language_url_split_prefix($path, $languages);
      if ($language !== FALSE) {
        $url_language = $language->language;
      }
      break;

    case LOCALE_LANGUAGE_NEGOTIATION_URL_DOMAIN:
      foreach ($languages as $language) {
        // Skip check if the language doesn't have a domain.
        if ($language->domain) {
          // Only compare the domains not the protocols or ports.
          // Remove protocol and add http:// so parse_url works.
          $language_host = 'http://' . str_replace(array('http://', 'https://'), '', $language->domain);
          $language_host = parse_url($language_host, PHP_URL_HOST);
          if ($host == $language_host) {
            $url_language = $language->language;
            break;
          }
        }
      }
      break;
  }

  return $url_language;
}
